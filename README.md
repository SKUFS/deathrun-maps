# deathrun-maps

Repository for the maps on the Deathrun server. If you wish for a new map to be added, simply create an issue or do a merge request.